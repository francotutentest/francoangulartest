import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes} from '@angular/router';
import { HttpClientModule, HttpClient} from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { from } from 'rxjs';
import { BookingListComponent } from './booking-list/booking-list.component';
import { MenuComponent } from './menu/menu.component';
import { BookingFilter } from './pipes/booking-filter.pipe';

const appRoutes: Routes = [
  {path:'login', component: LoginComponent},
  {path:'bookings', component: BookingListComponent},
  {path: '', redirectTo: 'login', pathMatch: 'full'},
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BookingListComponent,
    MenuComponent,
    BookingFilter
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes, {enableTracing: true})
  ],
  providers: [BookingFilter],
  bootstrap: [AppComponent]
})
export class AppModule { }
