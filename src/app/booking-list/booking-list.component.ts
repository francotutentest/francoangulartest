import { Component, OnInit } from '@angular/core';
import { BookingService } from '../core/booking.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-booking-list',
  templateUrl: './booking-list.component.html',
  styleUrls: ['./booking-list.component.css'],
  providers: [BookingService]
})
export class BookingListComponent implements OnInit {

  filterForm = this.formBuilder.group({
    bookingID : [''],
    bookingIDCondition: [''],
    bookingPrice : [''],
    bookingPriceCondition: ['']
  });
  bookings = [];

  constructor(private bookingService: BookingService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.getBookings();
  }

  getBookings() {
    this.bookingService.getBookings()
    .subscribe(
      (response:any) => {
        console.log(response);
        this.bookings = response;
      },
      (error:any) => {
        console.log(error);
      }
    );
  }

}
