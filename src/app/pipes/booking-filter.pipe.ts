import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'bookingFilter'})
export class BookingFilter implements PipeTransform {
  transform(booking: any[], bookingID?: string, bookingCompare?: string, bookingPrice?: string, bookingPriceCompare?:string): any[] {
    let result = booking;

    if(bookingID != undefined && bookingID != '') {
        if(bookingCompare == undefined || bookingCompare == '') {
            result = result.filter(element => element.bookingId.toString().includes(bookingID) );
        }
        
        if(bookingCompare == '>=') {
            result = result.filter(element => element.bookingId >= Number(bookingID));
        }

        if(bookingCompare == '<=') {
            result = result.filter(element => element.bookingId <= Number(bookingID));
        }
    }

    if(bookingPrice != undefined && bookingPrice != '') {
      if(bookingPriceCompare == undefined || bookingPriceCompare == '') {
          result = result.filter(element => element.bookingPrice.toString().includes(bookingPrice) );
      }
      
      if(bookingPriceCompare == '>=') {
          result = result.filter(element => element.bookingPrice >= Number(bookingPrice));
      }

      if(bookingPriceCompare == '<=') {
          result = result.filter(element => element.bookingPrice <= Number(bookingPrice));
      }
  }
    return result;
  }
}