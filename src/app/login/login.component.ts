import { Component, OnInit } from '@angular/core';
import { AuthService } from '../core/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthService]
})
export class LoginComponent implements OnInit {

  loginForm = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required]
  });
  title = 'Login';

  credentials = {
    email: '',
    pass: ''
  }

  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
  }

  login() {
    this.authService.login(encodeURIComponent(this.loginForm.value.email), this.loginForm.value.password)
    .subscribe(
      (response: any) => {
        console.log(response);
        localStorage.setItem('token', response.sessionTokenBck);
        localStorage.setItem('firstName', response.firstName);
        localStorage.setItem('lastName', response.lastName);
        localStorage.setItem('email', response.email);
        localStorage.setItem('phoneNumber', response.phoneNumber);
        localStorage.setItem('lastLogin', response.lastLogin);
        localStorage.setItem('userRole', response.userRole.userRole);
        this.router.navigate(['/bookings']);
      },
      (error: any) => {
        console.log(error);
      }
    )
  }

}
