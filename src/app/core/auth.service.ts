import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};

@Injectable()
export class AuthService {
    // private headers = new Headers({'Content-Type': 'application/json'});
    // private configHeader: any;

    constructor(private http: HttpClient) {
        //this.configHeader = { headers: this.headers };
    }

    login(email: string, pass: string) {
        httpOptions.headers = httpOptions.headers.set('App', environment.app);
        httpOptions.headers = httpOptions.headers.set('Password', pass);
        return this.http.put(environment.url_base + email, null, httpOptions);
    }
}
