import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    })
};

@Injectable()
export class BookingService {

    bookins = [];

    constructor(private http: HttpClient) {
    }

    getBookings() {
        //localStorage.getItem('token');
        //localStorage.getItem('email');
        
        httpOptions.headers = httpOptions.headers.set('App', environment.app);
        httpOptions.headers = httpOptions.headers.set('Token', localStorage.getItem('token'));
        httpOptions.headers = httpOptions.headers.set('Adminemail', localStorage.getItem('email'));
        
        return this.http.get(environment.url_base + encodeURIComponent(environment.email_booking) + environment.endpoint_booking, httpOptions);


        
    }
}