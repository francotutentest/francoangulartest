import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  userName: String;
  lastLogin: String;
  userRole: String;
  constructor() { }

  ngOnInit(): void {
    this.userName = localStorage.getItem('firstName') + ' ' + localStorage.getItem('lastName');
    this.lastLogin = localStorage.getItem('lastLogin');
    this.userRole = localStorage.getItem('userRole');
  }

}
