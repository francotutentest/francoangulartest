# TutenIssue3

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.2. 

It was develop by Franco Flores, Software developer who's try to get a job as a Developer in Tuten Company. This is an Angular 9 Test.

## Minimal requirements.

This project was teste with 
- NodeJS version 12.15.0
- NPM version 6.13.4

## Instalation
The firs step after clone this project is execute the next command.

`npm install`

To download all packages than projects needs.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Contact
francofloresdelgado@gmail.com
+58 414 761 23 97
skype: f.flores.d
